package org.clyze.doop.soot;

import org.clyze.doop.common.BasicJavaSupport;
import org.clyze.doop.common.Database;
import org.clyze.doop.common.JavaFactWriter;
import org.clyze.doop.common.PredicateFile;
import org.clyze.doop.common.SessionCounter;
import soot.*;
import soot.jimple.*;
import soot.jimple.internal.JimpleLocal;
import soot.jimple.toolkits.typing.fast.BottomType;
import soot.tagkit.*;
import soot.util.backend.ASMBackendUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static org.clyze.doop.common.JavaRepresentation.*;
import static org.clyze.doop.common.PredicateFile.*;


class DiggerFactWriter extends JavaFactWriter {
	private final Representation _rep;
    private final FactWriter _writer;
    private final Map<String, Type> _varTypeMap = new ConcurrentHashMap<>();
    private final boolean _reportPhantoms;
    private final Set<Object> seenPhantoms = new HashSet<>();

    DiggerFactWriter(FactWriter writer, Database db, boolean moreStrings, boolean artifacts,
               Representation rep, boolean reportPhantoms) {
        super(db, moreStrings,artifacts);
        _writer = writer;
        _rep = rep;
        _reportPhantoms = reportPhantoms;
    }


    void writeInstructionLine(SootMethod m, Unit u, Session session){
        Set<Integer> indexes;
        try {
        	indexes = session.getUnitNumbers(u);
        } catch (RuntimeException e) {
        	System.err.println(e.toString());
        	return;
        }
        String methodId = m.toString();
        int lineNumber = u.getJavaSourceStartLineNumber();
        SootClass mClass = m.getDeclaringClass();
        SourceFileTag tag = (SourceFileTag) mClass.getTag("SourceFileTag");
        String sourcefile = tag.getSourceFile();
        for(int index : indexes){
            _db.add(INSTRUCTION_LINE, methodId, str(index), str(lineNumber), sourcefile);
        }   
    }

    void writeLoadArrayIndexConstant(SootMethod m, Stmt stmt, Local base, Local to, IntConstant arrIndex, Session session) {
        writeLoadOrStoreArrayIndexConstant(m, stmt, base, to, arrIndex, session, LOAD_ARRAY_INDEX);
    }

    void writeStoreArrayIndexConstant(SootMethod m, Stmt stmt, Local base, Local from, IntConstant arrIndex, Session session) {
        writeLoadOrStoreArrayIndexConstant(m, stmt, base, from, arrIndex, session, STORE_ARRAY_INDEX);
    }

    private void writeLoadOrStoreArrayIndexConstant(SootMethod m, Stmt stmt, Local base, Local var, IntConstant arrIndex, Session session, PredicateFile predicateFile) {
        int index = session.calcUnitNumber(stmt);
        String insn = _rep.instruction(m, stmt, index);
        String methodId = _writer.writeMethod(m);

        Local indexLocal = _writer.writeNumConstantExpression(m, stmt, (NumericConstant) arrIndex, session);
        
        _db.add(predicateFile, insn, str(index), _rep.local(m, var), _rep.local(m, base), methodId);

        if (arrIndex != null)
            _db.add(ARRAY_INSN_INDEX, insn, _rep.local(m, indexLocal));
    }

    void writeThrows(SootMethod m, SootClass thrown) {
        _db.add(THROWS, m.toString(), thrown.getName());
    }
}